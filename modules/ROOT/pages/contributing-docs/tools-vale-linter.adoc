= Check Your Documentation Using Vale   
Fedora Documentation Team <https://discussion.fedoraproject.org/tag/docs>
v0.0.1, 2022-12-17

Before creating a merge request for any documentation or documentation updates, you will need to run https://vale.sh[Vale] on your text.     

Vale is a command line tool that checks your text for adherence to a defined style guide. Fedora documentation uses the https://stylepedia.net/style/5.1/[Red Hat] style guide. 

==  Install Vale on your computer 

You can install Vale on your computer, or use a containerized version with Podman.

To install Vale on your computer use one of the available installers for https://vale.sh/docs/vale-cli/installation/[Windows, macOS, and Linux].

If you are using Fedora, you can install from the `mczernek/vale` Copr repository. Be aware that Copr is not officially supported by Fedora infrastructure. Use packages at your own risk.

----
$ sudo dnf copr enable mczernek/vale && sudo dnf install vale
----

You can use the containerized version of Vale if you don't want to install it locally.

----
podman run --rm -v ${PWD}:/docs -w /docs jdkato/vale:latest <your_file.adoc>
----

If you installed Vale locally, verify that Vale is available by using the `vale -v` command:

----
$ vale -v
vale version 2.21.2 (your version may be different depending on when you installed Vale) 
----

The Red Hat style guide and the required Vale configuration files are already installed and available in the repository you forked from the main https://gitlab.com/fedora/docs[Fedora Docs] page in GitLab. 

You can verify this by checking for a `.vale.ini` file in the file list of your fork in your GitLab account. You do not need to download or install any additional style files. 

== Use Vale to check files or directories

To use Vale, do the following: 

. Create or edit a file in your local Fedora Docs repository. See xref:contributing-docs/tools-local-authoring-env.adoc[How to create and use a local Fedora authoring environment] for instructions on how to fork, clone, and manage Git repositories on your computer.
. Run the Vale linter on the file. You can run Vale on a single file, several files, and directories. 
. Make updates to the file to fix any errors, warnings, or suggestions.
. Rerun the Vale linter to verify the file passes.

When your file passes, you can commit your work and push to your fork on GitLab, and open a merge request. 

=== Vale commands and output

To run Vale on a single file:   

----
$ vale filename
----

To run Vale on several files:

----
$ vale filename 1 filename 2
----

To run Vale on all the files in a directory: 

----
$ vale directoryname/
----

Vale will return a list of results showing the location in the file, the level of severity, a hint about how to correct the result, and what style reference flagged the result. For example:

----
11:1   suggestion  Define acronyms and             RedHat.Definitions   
                    abbreviations (such as 'TOC')                        
                    on first occurrence if they're                       
                    likely to be unfamiliar.                             
15:54  error       Use 'for example' rather than   RedHat.TermsErrors   
                    'e.g.'.                                            
15:59  warning     Use correct American English    RedHat.Spelling      
                    spelling. Did you really mean                      
                    'Quickdocs'?
----

If a file returns a long list of results, use: 

----
$ vale --no-wrap filename
----

This will print each result on one line. This is also useful when checking many files or all the files in a directory. 

Vale has three levels of results that it will list at the bottom of the output: error, warning, and suggestion.   

`✖ 1 error, 3 warnings and 4 suggestions in 1 file.`  

- **error**: This is a blocker, and you must fix any errors found in the file.
- **warning**: This is a not a blocker, but is something that you need to fix to conform to the Red Hat style guide. 
- **suggestion**: This is not a blocker, but is something you must review and try to fix to conform to the Red Hat style guide.

If you want to only look for a specific result, use the `--minAlertLevel level` flag. This is useful if you have a long list of results and want to work on one specific result level at a time. 

`--minAlertLevel suggestion` (shows suggestion, warning, and error)

`--minAlertLevel warning` (shows warning and error)  

`--minAlertLevel error` (shows only error)  

For example, to only shows results flagged with error in the file, use:

----
$ vale --no-wrap --minAlertLevel error filename
----

== How to interpret Vale results 

Vale shows the results by line number and position (sometimes called a column). It also shows the style guideline that flagged the content. 

For example, this error is on line 15, starting at position 54  

----
15:54  error    Use 'for example' rather than 'e.g.'.        RedHat.TermsErrors
----

----
15 ** Editor in chief for specific documentation areas, e.g. Quickdocs
                                                        ^ position 54
----                                                       

Verify that your text editor shows line numbers and position. Most text editors will have a way to enable this view. 

Vale will usually tell you exactly what you need to do to fix the line, which in this case is to use 'for example' instead of 'e.g.'

Rerun Vale to verify that the error is now resolved. Continue running Vale to clear all errors, warnings, and suggestions. 

=== How to find guidance for correcting Vale results 

Every result returned by Vale shows the Red Hat style reference that flagged the word or phrase. This is at the end of the each result, in the format of `RedHat.style_name`. 

These files are in a `.vale` directory at the top level of your local repository, in a `styles/RedHat` subdirectory. 

All the style files point back to the reference guide maintained by Red Hat on the https://redhat-documentation.github.io/vale-at-red-hat/docs/main/reference-guide/reference-guide/[Vale for writers at Red Hat page]. 

You can usually find guidance there on what to change to resolve any error or warning level results. 

If you want to see the contents of a specific style file, you can examine the https://github.com/redhat-documentation/vale-at-red-hat/tree/main/.vale/styles/RedHat[vale-at-red-hat] page on GitHub. 

If you still have questions or need help deciding what changes to make, post a question in the Fedora Documentation room on https://chat.fedoraproject.org/#/room/#docs:fedoraproject.org[Fedora Chat]. 

== More information

Vale documentation: https://vale.sh/docs/  

Red Hat Technical Writing Style Guide: https://stylepedia.net/   

Guidelines for Red Hat Documentation: https://redhat-documentation.github.io/   

Fedora Docs Style Guide: https://docs.fedoraproject.org/en-US/fedora-docs/contributing-docs/style-guide/  

This guide shows how to use Vale from the command line, but https://docs.gitlab.com/ee/development/documentation/testing.html#configure-editors[plugins or packages] are available for several common text editors. 
